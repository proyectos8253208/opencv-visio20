@echo off
color b
echo ************************
echo *  Creador de vectores *
echo ************************
echo.
echo -----------------------------
echo Archivos de vector ya creados
echo -----------------------------
dir /B /c vector
echo -----------------------------
echo.
echo ****************************************
echo Cuidado: un archivo con el mismo nombre 
echo borrara el archivo ya creado
echo ****************************************
echo Inserte el nombre del archivo de salida de vector sin la extension
set /p archivo=
echo Ruta de salida: vector/%archivo%.vec
echo ************************
echo *   Archivos info.txt  *      
echo ************************
echo 
echo --------
echo Archivos
echo --------
type positive\info.txt | find /v /c "" 
echo --------
echo ************************
pause
echo N de archivos
set /p sel=
pause
cls
color c
echo.
echo.
echo ************************
echo Ruta de salida: vector/%archivo%.vec
echo Archivos: %sel%
echo ************************
echo Creacion de vectores
echo Cancelar: 0 Seguir: Otra tecla
set /p salir=
echo ************************
if %salir%==0 goto exit
cls
echo.
echo.
echo **********************************************************************
color a
pause
echo Borrado de cascadas anteriores (paso fundamental)
DEL /F /A cascades\*
echo haartraining.exe -data cascades -vec vector/%archivo%.vec -bg negative/bg.txt -npos %sel% -nneg 200 -nstages 15 -mem 2048 -mode ALL -w 24 -h 24 rem -nonsym > haarTraining.bat
createsamples.exe -info positive/info.txt -vec vector/%archivo%.vec -num %sel% -w 24 -h 24
pause
color E
echo **********************************************************************
start haarTraining.bat
echo Archivo de vectores realizado
echo Se ha guardado satisfactoriamente en la ruta vector/%archivo%.vec
echo **********************************************************************
pause
