import os
import cv2
import numpy as np
from xml.etree import ElementTree

def cascade():
    # Determinación del directorio
    directorio = os.path.join(os.getcwd(), "data")
    archivos = os.listdir(directorio)
    cascade = []
    name = []
    bgcolor = []
    color = []

    # creación de los objetos face_cascade como array de objetos
    for archivo in archivos:
        archivolect = os.path.join(directorio, archivo)
        cascade.append(cv2.CascadeClassifier(archivolect))
        
        cascadeName = ''
        with open(archivolect, 'rt') as f:
            tree = ElementTree.parse(f)
        for node in tree.iter('opencv_storage'):
            cascadeName = node.attrib.get('name')
            cascadeBgcolor = node.attrib.get('bgcolor')
            cascadecolor = node.attrib.get('color')
        name.append(cascadeName)  
        bgcolor.append(cascadeBgcolor) 
        color.append(cascadecolor) 

    return cascade, name, bgcolor, color

def cam(cascade, name, bgcolor, color):
    window = {'width': 720, 'height': 720}
    basicColor = {'green': (0, 255, 0), 'red': (0, 0, 255), 'blue': (255, 0, 0), 'black': (0, 0, 0), 'grey': (150, 150, 150), 'white': (255, 255, 255)}
    detectados = {'x': 0, 'y': 0, 'w': 0, 'h': 0}
    objetos = []
                
    # inicialización de videocámara 
    cap = cv2.VideoCapture(0)

    salidavideo = input("Salida de video s:si n:no: ")
    while salidavideo != 's' and salidavideo != 'n':
        salidavideo = input("Salida de video s:si n:no: ")

    grabacionvideo = input("Inicio de grabación s:si n:no: ")
    while grabacionvideo != 's' and grabacionvideo != 'n':
        grabacionvideo = input("Inicio de grabación s:si n:no: ")
    
    if grabacionvideo == 's':
        out = cv2.VideoWriter('recog.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 100, (window['width'], window['height']))
    
    while True:
        objetos = []
        valido, img = cap.read()
        if valido:
            img_gris = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
               
            for x in range(len(cascade)):
                detect = cascade[x].detectMultiScale(img_gris, 1.3, 5)
                name_object = name[x]
                color_object = color[x]
                bgcolor_object = bgcolor[x]
                for (x,y,w,h) in detect:
                    detectados = {'x':0,'y':0,'w':0,'h':0,'name':""}
                    xstr = x+(int(float(w)/2))
                    ystr = y+(int(float(h)/2))
                    cv2.rectangle(img,(x,y),(x+w,y+h),basicColor[bgcolor_object],1)
                    cv2.rectangle(img,(x+90,y+20),(x,y),basicColor[bgcolor_object],-1)
                    cv2.putText(img, name_object, ((x+5,y+15)), cv2.FONT_HERSHEY_DUPLEX, 0.5, basicColor[color_object], 1, cv2.LINE_AA)
                    
                    detectados['x'] = x
                    detectados['y'] = y
                    detectados['w'] = w
                    detectados['h'] = h
                    detectados['name'] = name_object
                    objetos.append(detectados)            
            img = cv2.resize(img, (window['width'], window['height']))
            
            if salidavideo == 's':
                cv2.imshow('img', img)
                
            print(objetos)
            
            if grabacionvideo == 's':
                out.write(img)
            
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        
        # Verificar si la ventana está cerrada
        if cv2.getWindowProperty('img', cv2.WND_PROP_VISIBLE) < 1:
            break
    
    cap.release()
    cv2.destroyAllWindows()

cascade, name, bgcolor, color = cascade()
cam(cascade, name, bgcolor, color)
